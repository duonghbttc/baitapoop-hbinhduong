import Service.Service;
import java.util.Scanner;

public class Main {

    public static void main(String args[]) throws Exception {

        Service service = new Service();
        Scanner sc = new Scanner(System.in);
        boolean exit = false;
        int luachon = 0;
        showMenu();
        while (true) {
            luachon = sc.nextInt();
            switch (luachon) {
                case 1:
                    int KHVN = 0;
                    System.out.println("1. Thêm mới hóa đơn ");
                    System.out.println("2. Xem toàn bộ hóa đơn ");
                    System.out.println("3. Tổng số lượng tiêu thụ ");
                    System.out.println("4. Tổng hóa đơn trong tháng 7/2020");
                    KHVN = sc.nextInt();
                    switch (KHVN) {
                        case 1:
                            service.addVNCustomer();
                            break;
                        case 2:
                            service.showVNCustomer();
                            break;
                        case 3:
                            service.sumTotalVNCustomer();
                            break;
                        case 4:
                            service.showByMonthVNCustomer();
                            break;
                    }
                    break;

                case 2:
                    int KHNN = 0;
                    System.out.println("1. Thêm mới hóa đơn ");
                    System.out.println("2. Xem toàn bộ hóa đơn ");
                    System.out.println("3. Tổng số lượng tiêu thụ ");
                    System.out.println("4. Trung bình tiền ");
                    System.out.println("5. Các hóa đơn trong tháng 7/2020");
                    KHNN = sc.nextInt();
                    switch (KHNN) {
                        case 1:
                            service.addForeignCustomer();
                            break;
                        case 2:
                            service.showForeignCustomer();
                            break;
                        case 3:
                            service.sumTotalForeignCustomer();
                            break;
                        case 4:
                            service.averagedForeignCustomer();
                            break;
                        case 5:
                            service.showByMonthForeign();
                            break;
                    }
                    break;
                default:
                    System.out.println("invalid! please choose action in below menu:");
                    break;
            }
            if (exit) {
                break;
            }
            // show menu
            showMenu();
        }
    }

    public static void showMenu() {
        System.out.print("\n");
        System.out.print("1. Khách hàng trong nước. \n");
        System.out.print("2. Khách hàng nước ngoài. \n");
        System.out.print("<========================================================> \n");
    }
}
