package Model;

public class ForeignCustomer extends Customer {

    public ForeignCustomer() {};

    public ForeignCustomer(String customerID, String name, String billingDate, int amount, String nationality) {
        super(customerID, name, billingDate, amount);
        setNationality(nationality);
    }

    public double getTotalCost() {
        setPrice(1600);
        return super.getTotalCost();
    }

    public void display() {
        super.display();
    }
}
