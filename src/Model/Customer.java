package Model;

import Service.BillDate;

import java.util.Date;

public class Customer {
    private String customerID;
    private String name;
    private String nationality;
    private Date billingDate;
    private int amount;
    private int price;
    private int limit;
    private int totalCost;

    public Customer() {};

    public Customer(String customerID, String name, String billingDate, int amount) {
        this.customerID = customerID;
        this.name = name;
        this.amount = amount;
        this.billingDate = BillDate.SaveDate(billingDate);
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(Date billingDate) {
        this.billingDate = billingDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public double getTotalCost(){
        return price * amount;
    }

    public void display() {
        System.out.println("mã KH: " + customerID);
        System.out.println("tên KH: " + name);
        System.out.println("Quốc Tịch: " + nationality);
        System.out.println("Ngày xuất hóa đơn: " + BillDate.PrintDate(billingDate));
        System.out.println("Tháng xuất hóa đơn: " + BillDate.getMonth(billingDate));
        System.out.println("số kw tiêu thụ: " + amount);
        System.out.println("tổng thanh toán: " + getTotalCost() );
    }
}
