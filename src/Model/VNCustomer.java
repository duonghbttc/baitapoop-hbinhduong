package Model;

public class VNCustomer extends Customer {

    private String customerType;

    public VNCustomer() {};

    public VNCustomer(String customerID, String name,String billingDate, int amount, String customerType) {
        super(customerID, name, billingDate, amount);
        this.customerType = customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerType() {
        return customerType;
    }

    @Override
    public double getTotalCost() {
        setPrice(1600);
        setLimit(500);
        if(getLimit() >= getAmount()){
            return super.getTotalCost();
        }else{
            return super.getTotalCost() * getLimit() + getPrice() * 2.5 * (getAmount() - getLimit());
        }
    }

    @Override
    public void display() {
        super.display();
    }
}
