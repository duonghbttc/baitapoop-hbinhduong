package Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BillDate {

    public static Date SaveDate(String inputStringDate) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try{
            date = formatter.parse(inputStringDate);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }

    public static String PrintDate(Date inputDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormat = formatter.format(inputDate);
        return dateFormat;
    }

    public static int getMonth(Date inputDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM");
        String monthFormat = formatter.format(inputDate);
        int month = Integer.parseInt(monthFormat);
        return month;
    }

    public static int getYear(Date inputDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String monthFormat = formatter.format(inputDate);
        int year = Integer.parseInt(monthFormat);
        return year;
    }
}
