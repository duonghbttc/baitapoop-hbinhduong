package Service;

import Model.Customer;
import Model.ForeignCustomer;
import Model.VNCustomer;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Service {

    private ArrayList<ForeignCustomer> foreignCustomers = new ArrayList<ForeignCustomer>();
    private ArrayList<VNCustomer> vnCustomers = new ArrayList<VNCustomer>();

    public Customer addCustomer() {
        Scanner sc = new Scanner(System.in);
        Customer customer = new Customer();
        System.out.print("Nhập mã khách hàng: ");
        customer.setCustomerID(sc.next());
        System.out.print("Họ và tên: ");
        customer.setName(sc.next());
        System.out.print("Ngày ra hóa đơn(dd/MM/yyyy): ");
        String billingDate = sc.next();
        customer.setBillingDate(BillDate.SaveDate(billingDate));
        System.out.print("Số KW tiêu thụ: ");
        customer.setAmount(sc.nextInt());
        return customer;
    }

    public void addForeignCustomer() {
        Scanner sc = new Scanner(System.in);

        ForeignCustomer foreignCustomer = new ForeignCustomer();
        Customer customer = addCustomer();

        foreignCustomer.setCustomerID(customer.getCustomerID());
        foreignCustomer.setName(customer.getName());
        foreignCustomer.setBillingDate(customer.getBillingDate());
        foreignCustomer.setAmount(customer.getAmount());
        foreignCustomer.getTotalCost();
        System.out.print("Nhập Quốc tịch: ");
        foreignCustomer.setNationality(sc.next());

        this.foreignCustomers.add(foreignCustomer);
    }

    public void showForeignCustomer() {
        System.out.println("Danh sách khách hàng nước ngoài: ");
        for (int i = 0; i < foreignCustomers.size(); i++) {
            foreignCustomers.get(i).display();
        }
    }

    public void sumTotalForeignCustomer() {
        int sum= 0;
        for (int i = 0; i < foreignCustomers.size(); i++) {
            sum += foreignCustomers.get(i).getAmount();
        }
        System.out.println("Tổng lượng tiêu dùng của khách nước ngoài là: " + sum);
    }

    public void averagedForeignCustomer() {
        int result = 0;
        int count = 0;
        for (int i = 0; i < foreignCustomers.size(); i++) {
            result += foreignCustomers.get(i).getTotalCost();
            count ++;
        }
        System.out.println("Trung bình tiền của khách hàng nước ngoài là: " + (result/count));
    }

    public void showByMonthForeign() {
        ArrayList<ForeignCustomer> getCustomerByMonth = new ArrayList<ForeignCustomer>();
        for (int i = 0; i < foreignCustomers.size(); i++) {
            Date month = foreignCustomers.get(i).getBillingDate();
            if(BillDate.getMonth(month) == 7){
                if(BillDate.getYear(month) == 2020){
                    getCustomerByMonth.add(foreignCustomers.get(i));
                }
            }
        }
        System.out.println("Danh sách hóa đơn trong tháng 7/2020: ");
        for (int i = 0; i < getCustomerByMonth.size(); i++) {
            getCustomerByMonth.get(i).display();
        }
    }



    public void addVNCustomer() {
        Scanner sc = new Scanner(System.in);

        VNCustomer vnCustomer = new VNCustomer();
        Customer customer = addCustomer();
        vnCustomer.setCustomerID(customer.getCustomerID());
        vnCustomer.setName(customer.getName());
        vnCustomer.setBillingDate(customer.getBillingDate());
        vnCustomer.setAmount(customer.getAmount());
        vnCustomer.setNationality("Việt Nam");
        vnCustomer.getTotalCost();
        System.out.print("Nhập loại khách hàng: ");
        vnCustomer.setCustomerType(sc.next());

        this.vnCustomers.add(vnCustomer);
    }

    public void showVNCustomer() {
        System.out.println("Danh sách khách hàng trong nước: ");
        for (int i = 0; i < vnCustomers.size(); i++) {
            vnCustomers.get(i).display();
        }
    }

    public void sumTotalVNCustomer() {
        int sum = 0;
        for (int i = 0; i < vnCustomers.size(); i++) {
            sum += vnCustomers.get(i).getAmount();
        }
        System.out.println("Tổng lượng tiêu dùng của khách trong nước là: " + sum);
    }

    public void showByMonthVNCustomer() {
        ArrayList<VNCustomer> getCustomerByMonth = new ArrayList<VNCustomer>();
        for (int i = 0; i < vnCustomers.size(); i++) {
            Date month = vnCustomers.get(i).getBillingDate();
            if(BillDate.getMonth(month) == 7){
                if(BillDate.getYear(month) == 2020){
                    getCustomerByMonth.add(vnCustomers.get(i));
                }
            }
        }
        System.out.println("Danh sách hóa đơn trong tháng 7/2020: ");
        for (int i = 0; i < getCustomerByMonth.size(); i++) {
            getCustomerByMonth.get(i).display();
        }
    }
}
